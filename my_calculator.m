%MATLAB R2020b
%name:my_calculator
%author:kacpdeb877
%date: 13.11.2020
%version: v1.0

%zad1
disp('Zad 1:')
h_planck = 6.626*10^-34; %stała plancka [J*s]
wynik1 = h_planck/(2*pi);

disp(['h/(2π) = ', num2str(wynik1)])

%zad2
disp('Zad 2:')
liczba_e = 2.71828; %zdefiniowanie liczby Eulera jako zmiennej
wynik2 = sin((pi/4)/liczba_e);

disp(['sin(45°/e) = ', num2str(wynik2)])

%zad3
disp('Zad 3:')
wynik3 = (hex2dec('0x0098d6'))/(2.445*10^23); %wartość w liczniku należało najpierw zamienić z systemu szesnastkowego na dziesiętny

disp(['0x0098d6 / (1,445·10^23) = ', num2str(wynik3)])

%zad4
disp('Zad 4:')
wynik4 = sqrt(liczba_e - pi);

disp(['√e−π = ', num2str(wynik4)])

%zad5
disp('Zad 5:')
str_pi = num2str(pi,14); %utworzenie ciągu znaków składającego sie z liczby pi do 12 znaku
wynik5 = str_pi(14); %wyświetlenie 14 elemntu listy, w liście znajduje się dodatkowo "," i "3"

disp(['Dwunasta cyfra poprzecinkowa liczby π to: ', num2str(wynik5)])

%zad6
disp('Zad 6:')
time_now = clock; %zdefiniowanie aktualnego czasu

%zdefiniowanie daty narodzin jako zmienne
year = 1999;
month = 5;
day = 14;
hour = 17;

%odczytanie i przypisanie do zmiennych aktualnej daty
year_now = time_now(1);
month_now = time_now(2);
day_now = time_now(3);
hour_now = time_now(4);

%instrukcja warunkowa poptrzebna do obliczenia ilości dni które upłyneły w
%dawnym roku (wynika to z tego że miesiące mają różne długości)
if month_now == 1
    month = 0;
elseif month_now == 2
    month = 31;
elseif month_now == 3
    month = 31+28;
elseif month_now == 4
    month = 31*2+28;
elseif month_now == 5
    month = 31*2+28+30;
elseif month_now == 6
    month = 31*3+28+30;
elseif month_now == 7
    month = 31*3+28+30*2;
elseif month_now == 8
    month = 31*4+28+30*2;
elseif month_now == 9
    month = 31*5+28+30*2;
elseif month_now == 10
    month = 31*5+28+30*3;
elseif month_now == 11
    month = 31*6+28+30*3;
elseif month_now == 12
    month = 31*6+28+30*4;
end


leap_years = ceil((year_now-year)/4); %obliczanie ilości lat przestępnych (wynik musi być zaokrąglony w góre)

time_after_2000_h = (365-31-28-31-30-day)*24; %obliczenie ilości godzin od narodzin do roku 2000
time_befor_2000_y = year_now-2000; %obliczanie ilosci lat które upłyneły od 2000 roku
time_befor_2000_d = (time_befor_2000_y*365)+day_now+month; %obliczanie ilosci dni które upłyneły od 2000 roku
time_befor_2000_h = (time_befor_2000_d*24)+hour_now+(leap_years*24); %obliczanie ilosci godzin które upłyneły od 2000 roku (ilość lat przestępnych pomnożona przez 24)

wynik6 = time_befor_2000_h+time_after_2000_h; %sumowanie liczby godzin od narodzin do 2000 roku i od niego do teraz

disp(['Od moich narodzin mineło ', num2str(wynik6), ' godzin'])


%zad7
disp('Zad 7:')
mianownik = hex2dec('0x0aaff'); %zamiana liczby w mianowniku z systemu szesnastkowego na dziesiętnego
r_ziemi = 6371000; %zaokrąglony promień ziemi [m]
%liczba_e = 2.718281828;

potega = (sqrt(7)/2)-log(r_ziemi/(10^8)); %osobne policzenie potęgi liczby Eulera wystepującej w równaniu

wynik7 = atan((liczba_e^potega)/mianownik);

disp(['atan((liczba_e^(sqrt(7)/2)-log(r_ziemi/(10^8)))/0x0aaff)', num2str(wynik7)])

%zad8
disp('Zad 8:')
avo = 6.02214076e23; %zdefiniowanie liczby Avogadra
ilosc = (avo*(10^-6))/4; %obliczenie ile wynosi 1/4 μmola, czyli liczba cząstek alkoholu etylowego
atomy = 9*ilosc; %w każdej cząstce alkoholu etylowego znajduje się 9 atomów
wynik8 = atomy;

disp(['liczba atomów w  1/4 μmola alkoholu etylowego to', num2str(wynik8)])

%zad9
disp('Zad 9:')
cc = 2*ilosc; %obliczenie liości całego węgla w 1/4 μmola alkoholu etylowego
c13 = cc/101; %obliczenie ilości izotopu węgla C13 wiedząc że taki jest co stopierwszy

wynik9 = (c13/atomy)*1000; %obliczenie stosunku izotopu C13 do liczby wszystkich atomów a następnie przekształcenie wyniku na promile.

disp(['W alkoholu etylowym izotop C13 stanowi ', num2str(wynik9), '‰ wszystkich atomów'])